/*
 * Copyright 2014 Marc Nause <marc.nause@gmx.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see  http:// www.gnu.org/licenses/.
 */
package de.audioattack.openlink.ui;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import de.audioattack.openlink.BuildConfig;
import de.audioattack.openlink.R;

public class MainActivity extends Activity {

    @Override
    protected final void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Button btnVideo = findViewById(R.id.btn_video);
        btnVideo.setOnClickListener(v -> openUri(Uri.parse(getString(R.string.url_video))));

        final Button btnSource = findViewById(R.id.btn_source_code);
        btnSource.setOnClickListener(v -> openUri(Uri.parse(getString(R.string.url_source))));

        final TextView tvVersion = findViewById(R.id.version);
        tvVersion.setText(getString(R.string.lbl_version, BuildConfig.VERSION_NAME));

        final TextView tvCopyright = findViewById(R.id.copyright);
        tvCopyright.setText(fromHtml(getString(R.string.tv_copyright)));
        tvCopyright.setMovementMethod(LinkMovementMethod.getInstance());

    }

    private void openUri(final Uri uri) {
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, uri));
        } catch (SecurityException | ActivityNotFoundException ex) {
            Toast.makeText(getApplicationContext(), getString(R.string.error_unable_to_open, uri.toString()), Toast.LENGTH_LONG)
                    .show();
        }
    }

    /**
     * Wrapper for Html.fromHtml which uses deprecated version for older API levels.
     *
     * @param html HTML representation of text
     * @return displayable styled text
     */
    private static Spanned fromHtml(final String html) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            //noinspection deprecation
            return Html.fromHtml(html);
        }
    }

}
